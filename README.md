# OpenLPR wrapper in Node.js

Unofficial Node.js Wrapper for OpenLPR License Plate Recognition HTTP API.
Wrapper by Shinobi Systems (https://shinobi.video)

OpenLPR : https://github.com/faisalthaheem/open-lpr

```
const apiHost = 'http://localhost:5000'
const openLPR = require('node-openlpr')

const {
    detectPlate,
    apiRequest,
    uploadFile,
} = openLPR(apiHost);
```

Example of `/process` with file buffer.

```
const fileBuffer = fs.readFileSync(filePath);
const file = {
   data: fileBuffer,
   name: 'example.jpg',
   comment: 'Sample file',
}
detectPlate(file).then((data) => {
    console.log(data)
})
```

Example of `/process` with createReadStream.

```
const file = {
   data: fs.createReadStream(filePath),
   name: 'example.jpg',
   comment: 'Sample file',
}
detectPlate(file).then((data) => {
    console.log(data)
})
```
